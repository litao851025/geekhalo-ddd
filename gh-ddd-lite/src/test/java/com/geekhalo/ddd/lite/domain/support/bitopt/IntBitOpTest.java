package com.geekhalo.ddd.lite.domain.support.bitopt;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntBitOpTest {
    private Order order = new Order();

    @Test
    public void match() {
        {
            Assert.assertFalse(this.order.isGaotu());
            this.order.setGaotu(true);
            Assert.assertTrue(this.order.isGaotu());
            this.order.setGaotu(false);
            Assert.assertFalse(this.order.isGaotu());
        }

        {
            Assert.assertFalse(this.order.isTutu());
            this.order.setTutu(true);
            Assert.assertTrue(this.order.isTutu());
            this.order.setTutu(false);
            Assert.assertFalse(this.order.isTutu());
        }

        {

            System.out.println(this.order.filterByTutu("app"));

            System.out.println(this.order.filterByGaotu("app"));

            System.out.println(this.order.filterByTutuOrGaotu("app"));

            System.out.println(this.order.filterByTutuAndGaotu("app"));


            System.out.println(this.order.filterByNotTutu("app"));

        }
    }

    @Test
    public void testGetByBitIndex(){
        for (int i = 1; i<=32 ;i++){
            IntMaskOp intMaskOp = IntMaskOp.getByBitIndex(i);
            Assert.assertNotNull(intMaskOp);
        }
    }
}