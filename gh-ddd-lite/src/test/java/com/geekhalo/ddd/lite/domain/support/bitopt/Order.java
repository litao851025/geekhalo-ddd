package com.geekhalo.ddd.lite.domain.support.bitopt;

public class Order {
    private static final IntMaskOp TUTU = IntMaskOp.MASK_1;
    private static final IntMaskOp GAOTU = IntMaskOp.MASK_2;

    private int apps = 0;

    public void setTutu(boolean tutu){
        this.apps = TUTU.set(this.apps, tutu);
    }

    public boolean isTutu(){
        return TUTU.isSet(this.apps);
    }

    public void setGaotu(boolean gaotu){
        this.apps = GAOTU.set(this.apps, gaotu);
    }

    public boolean isGaotu(){
        return GAOTU.isSet(this.apps);
    }

    public String filterByGaotu(String filed){
        return GAOTU.toSqlFilter(filed);
    }

    public String filterByTutu(String filed){
        return TUTU.toSqlFilter(filed);
    }

    public String filterByTutuOrGaotu(String field){
        return TUTU.or(GAOTU).toSqlFilter(field);
    }

    public String filterByTutuAndGaotu(String field){
        return TUTU.and(GAOTU).toSqlFilter(field);
    }

    public String filterByNotTutu(String field){
        return TUTU.not().toSqlFilter(field);
    }
}
